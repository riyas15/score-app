package com.example.scoreapplication.data.base

import android.content.Context
import com.example.scoreapplication.data.model.MatchModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object DataEngine {
    fun readJsonFromAssets(context: Context): String {
        return context.assets.open("status.json").bufferedReader().use { it.readText() }
    }
    fun parseJsonToModel(jsonString: String): MatchModel {
        val gson = Gson()
        return gson.fromJson(jsonString, object : TypeToken<MatchModel>() {}.type)
    }

    fun getMatchData(context: Context) : MatchModel{
        return parseJsonToModel(readJsonFromAssets(context))
    }
}