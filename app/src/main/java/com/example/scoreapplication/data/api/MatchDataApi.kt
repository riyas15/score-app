package com.example.scoreapplication.data.api

import android.content.Context
import com.example.scoreapplication.data.model.DetailSummery
import com.example.scoreapplication.data.model.HomeSummery
import com.example.scoreapplication.data.model.MatchModel
import kotlinx.coroutines.flow.Flow

interface MatchDataApi {

   suspend fun readMatchData(): Flow<MatchModel>

   suspend fun getHomeSummery() : Flow<HomeSummery>

   suspend fun getHomeDetilsSummery() : Flow<DetailSummery>
}