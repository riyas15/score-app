package com.example.scoreapplication.data.repo

import android.content.Context
import com.example.scoreapplication.data.api.MatchDataApi
import com.example.scoreapplication.data.base.DataEngine
import com.example.scoreapplication.data.model.BatsmanData
import com.example.scoreapplication.data.model.Bowler
import com.example.scoreapplication.data.model.BowlerData
import com.example.scoreapplication.data.model.DetailSummery
import com.example.scoreapplication.data.model.HomeSummery
import com.example.scoreapplication.data.model.Innings
import com.example.scoreapplication.data.model.MatchModel
import com.example.scoreapplication.data.model.Team
import com.example.scoreapplication.data.model.TeamData
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.math.abs

class MatchRepo @Inject constructor(
    @ApplicationContext private val context: Context
) : MatchDataApi {

    private val apiData = DataEngine.getMatchData(context)
    private var homeSummery: HomeSummery = HomeSummery()
    private var detailSummery: DetailSummery = DetailSummery()

    override suspend fun readMatchData(): Flow<MatchModel> {
        detailSummery.toss = apiData.matchDetails.toss

        return flow {
            emit(
                apiData
            )
        }
    }

    override suspend fun getHomeDetilsSummery(): Flow<DetailSummery> = withContext(Dispatchers.IO) {
        setupDetailsSummery()
        flow {
            emit(detailSummery)
        }
    }


    override suspend fun getHomeSummery(): Flow<HomeSummery> = withContext(Dispatchers.IO) {
        setupHomeSummery()
        flow {
            emit(homeSummery)
        }
    }

    private fun setupHomeSummery() {
        // Map parsed JSON data to HomeSummary
        homeSummery = HomeSummery().apply {
            matchType = apiData.matchDetails.format
            titleTeamA = apiData.matchDetails.teams[0].name
            titleTeamB = apiData.matchDetails.teams[1].name
            totalRunTeamA = apiData.matchDetails.teams[0].players.sumBy { it.runs }
            totalRunTeamB = apiData.matchDetails.teams[1].players.sumBy { it.runs }
            wicketsOfTeamA = apiData.matchDetails.fallOfWickets
                .firstOrNull { it.team == titleTeamA }?.wickets?.size ?: 0
            wicketsOfTeamB = apiData.matchDetails.fallOfWickets
                .firstOrNull { it.team == titleTeamB }?.wickets?.size ?: 0
            totalOverTeamA = apiData.matchDetails.teams[0].bowlers.sumOf { it.overs.toDouble() }
            totalOverTeamB = apiData.matchDetails.teams[1].bowlers.sumOf { it.overs.toDouble() }
            margin = abs(totalRunTeamA - totalRunTeamB)
            winningTeam = if (totalRunTeamA > totalRunTeamB) titleTeamA else titleTeamB
        }

        // Print or use the homeSummary object
        println(homeSummery)
    }

    private fun setupDetailsSummery() {
        // Map parsed JSON data to DetailSummary
        detailSummery = DetailSummery().apply {
            type = apiData.matchDetails.format
            toss = apiData.matchDetails.toss
            toss_decision = apiData.matchDetails.toss_decision
            val scoreTeamA = apiData.matchDetails.teams[0].players.sumBy { it.runs }
            val scoreTeamB = apiData.matchDetails.teams[1].players.sumBy { it.runs }
            val winningScore = maxOf(scoreTeamA, scoreTeamB)
            apiData.matchDetails.teams.forEachIndexed { index, team ->
                val teamData = TeamData().apply {
                    name = team.name
                    score = team.players.sumBy { it.runs }
                    wicket = apiData.matchDetails.fallOfWickets
                        .firstOrNull { it.team == team.name }?.wickets?.size ?: 0
                    overs = team.bowlers.sumByDouble { it.overs.toDouble() }
                    isToss = (index == 0 && team.name == apiData.matchDetails.toss)
                    isWin = (score == winningScore)
                }
                teams.add(teamData)
            }
            firstInnings = mapInnings(apiData.matchDetails.teams[0])
            secondInnings = mapInnings(apiData.matchDetails.teams[1])
        }

        // Print or use the detailSummary object
        println(detailSummery)
    }

    private fun mapInnings(team: Team): Innings {
        val innings = Innings()
        innings.batsmanList.addAll(team.players.map {
            BatsmanData(
                it.name,
                it.runs,
                it.runs.toDouble() / it.balls,
                it.balls,
                it.balls / 6.0,
                it.dismissal.type,
                it.fours,
                it.sixes,
                dismissalByBowler = BowlerData(it.dismissal.bowler),
                dissmissBy = it.dismissal.fielder ?: ""
            )
        })
        innings.bowlerList.addAll(team.bowlers.map {
            val maidenOvers = calculateMaidenOvers(it)
            BowlerData(
                name = it.name,
                bowledOvers = it.overs.toDouble(),
                runsConsume = it.runsConceded,
                wickets = it.wickets,
                economy = it.runsConceded.toDouble() / it.overs,
                maidenOvers = maidenOvers
            )
        })
        return innings
    }
    fun calculateMaidenOvers(bowler: Bowler): Int {
        // Assuming a maiden over if no runs are conceded and no wicket is taken
        return if (bowler.runsConceded == 0 && bowler.wickets == 0) 1 else 0
    }

}