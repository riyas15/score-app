package com.example.scoreapplication.data.model

data class HomeSummery(
    var matchType:String = "",
    var titleTeamA:String = "",
    var titleTeamB:String = "",
    var totalRunTeamA:Int = 0,
    var totalRunTeamB:Int = 0,
    var wicketsOfTeamA:Int = 0,
    var wicketsOfTeamB:Int = 0,
    var totalOverTeamA:Double = 0.0,
    var totalOverTeamB:Double = 0.0,
    var margin:Int = 0,
    var winningTeam:String = ""
)

data class BatsmanData(
    var name:String = "",
    var run:Int =0,
    var statics:Double = 0.0,
    var ballsFace:Int = 0,
    var overFace:Double = 0.0,
    var dismissal:String = "",
    var fours: Int = 0, // Number of fours
    var sixes: Int = 0, // Number of sixes
    var dismissalByBowler:BowlerData = BowlerData(),
    var dissmissBy:String = ""
)

data class BowlerData(
    var name:String = "" ,
    var bowledOvers : Double = 0.0,
    var runsConsume : Int = 0,
    var wickets : Int = 0,
    var economy : Double = 0.0,
    var maidenOvers : Int = 0
)

data class DetailSummery(
    var type:String = "",
    var isDummy:Boolean = true,
    var toss:String = "",
    var toss_decision :String = "",
    var teams:ArrayList<TeamData> = arrayListOf(),
    var firstInnings:Innings = Innings(),
    var secondInnings:Innings = Innings()
)

data class TeamData(
    var name:String = "",
    var score:Int = 0,
    var wicket:Int = 0,
    var overs:Double = 0.0,
    var isToss :Boolean = false,
    var isWin : Boolean = false

)
data class Innings(
    var batsmanList:ArrayList<BatsmanData> = arrayListOf(),
    var bowlerList: ArrayList<BowlerData> = arrayListOf()
)
