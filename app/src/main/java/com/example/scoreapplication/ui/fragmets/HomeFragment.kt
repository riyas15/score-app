package com.example.scoreapplication.ui.fragmets

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.scoreapplication.data.model.HomeSummery
import com.example.scoreapplication.databinding.FragmentHomeBinding
import com.example.scoreapplication.ui.activity.HomeActivity
import com.example.scoreapplication.viewmoel.MainViewModel


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HomeFragment : Fragment()  {

    private lateinit var _binding: FragmentHomeBinding
    private val binding get() = _binding

    private val viewModel: MainViewModel by activityViewModels()


    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()

        binding.match.setOnClickListener {
            ( requireActivity() as HomeActivity).setCurrentFragment(SummeryDetailsFragment())
        }
    }

    private fun setupView(homeSummery: HomeSummery) {

        binding.tvMatchType.text = "${homeSummery.matchType} League"
        binding.tvTitleA.text =  homeSummery.titleTeamA
        binding.tvTitleB.text =  homeSummery.titleTeamB
        binding.tvTotalRunA.text = "${homeSummery.totalRunTeamA}/${homeSummery.wicketsOfTeamA}"
        binding.tvTotalRunB.text = "${homeSummery.totalRunTeamB}/${homeSummery.wicketsOfTeamB}"
        binding.tvTotalOverA.text = "(${homeSummery.totalOverTeamA})"
        binding.tvTotalOverB.text = "(${homeSummery.totalOverTeamB})"
        binding.tvWinBy.text = "${homeSummery.winningTeam} win by ${homeSummery.margin} runs"


    }

    private fun observer() {
        viewModel.homePresentData.observe(requireActivity()){
            setupView(it)
            Log.d("winnnnn","${it.totalRunTeamA} === ${it.totalRunTeamB}  === Win By ${it.winningTeam}")
        }
     viewModel.homeData.observe(requireActivity()){
         Log.d("data2","${it.matchDetails.toss}")
     }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


}