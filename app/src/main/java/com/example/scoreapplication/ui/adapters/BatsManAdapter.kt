package com.example.scoreapplication.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.scoreapplication.data.model.BatsmanData
import com.example.scoreapplication.databinding.BatsmanItemBinding

class BatsManAdapter : RecyclerView.Adapter<BatsManAdapter.ViewHolder>() {

    lateinit var binding:BatsmanItemBinding
    var data:ArrayList<BatsmanData> = ArrayList()

    fun setList(data:ArrayList<BatsmanData>){
        this.data = data
    }

    inner class ViewHolder : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = BatsmanItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder()
    }

    override fun getItemCount(): Int  = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data[position].let {
            binding.tvBatName.text = it.name
            binding.tvBatRun.text = "${it.run}"
            binding.tvBatBall.text = "${it.ballsFace}"
            binding.tvBat4.text = "${it.fours}"
            binding.tvBat6.text = "${it.sixes}"
            binding.tvBatEco.text = "${it.statics}"
            binding.tvBatDiss.text = "${it.dismissal} by ${it.dissmissBy} | ${it.dismissalByBowler.name}"
        }
    }

}