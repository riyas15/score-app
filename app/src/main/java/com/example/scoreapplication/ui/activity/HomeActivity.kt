package com.example.scoreapplication.ui.activity

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.scoreapplication.R
import com.example.scoreapplication.databinding.ActivityHomeBinding
import com.example.scoreapplication.ui.fragmets.HomeFragment
import com.example.scoreapplication.viewmoel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    lateinit var binding : ActivityHomeBinding
    private val viewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setCurrentFragment(HomeFragment())
        observer()
    }

    private fun observer() {
        viewModel.homeData.observe(this){
            Log.d("data","${it.matchDetails.toss}")
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        Log.d("back","${supportFragmentManager.fragments.size}")
        if(supportFragmentManager.fragments.size == 0) {
            super.onBackPressed();
        }
        else {
            supportFragmentManager.popBackStack()
        }
    }

     fun setCurrentFragment(fragment: Fragment) =
         supportFragmentManager.beginTransaction().apply {
             replace(R.id.fragmentHolder,fragment)
                 .addToBackStack(null)
             commit()
         }
}


