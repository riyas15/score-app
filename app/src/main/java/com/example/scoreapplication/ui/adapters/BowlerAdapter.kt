package com.example.scoreapplication.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.scoreapplication.data.model.BatsmanData
import com.example.scoreapplication.data.model.BowlerData
import com.example.scoreapplication.databinding.BatsmanItemBinding
import com.example.scoreapplication.databinding.ItemBowlerBinding

class BowlerAdapter : RecyclerView.Adapter<BowlerAdapter.ViewHolder>() {

    lateinit var binding: ItemBowlerBinding
    var data:ArrayList<BowlerData> = ArrayList()

    fun setList(data:ArrayList<BowlerData>){
        this.data = data
    }

    inner class ViewHolder : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = ItemBowlerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder()
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       data[position].let {
           binding.tvBowName.text = it.name
           binding.tvBowOver.text = "${it.bowledOvers}"
           binding.tvBowMaidin.text = "${it.maidenOvers}"
           binding.tvBowRun.text = "${it.runsConsume}"
           binding.tvBowWicket.text = "${it.wickets}"
           binding.tvBowEco.text = "${it.economy}"

       }
    }
}