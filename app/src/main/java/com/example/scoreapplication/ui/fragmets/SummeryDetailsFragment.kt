package com.example.scoreapplication.ui.fragmets

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.scoreapplication.R
import com.example.scoreapplication.data.model.DetailSummery
import com.example.scoreapplication.databinding.FragmentHomeBinding
import com.example.scoreapplication.databinding.FragmentSummeryDetailsBinding
import com.example.scoreapplication.ui.adapters.BatsManAdapter
import com.example.scoreapplication.ui.adapters.BowlerAdapter
import com.example.scoreapplication.viewmoel.MainViewModel


class SummeryDetailsFragment : Fragment() {

    private lateinit var _binding: FragmentSummeryDetailsBinding
    private val binding get() = _binding
    private val batAdapterA: BatsManAdapter = BatsManAdapter()
    private val batAdapterB: BatsManAdapter = BatsManAdapter()
    private val bowlAdapterA: BowlerAdapter = BowlerAdapter()
    private val bowlAdapterB: BowlerAdapter = BowlerAdapter()


    private val viewModel: MainViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSummeryDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observers()
        binding.expandA.setOnClickListener {
            binding.ViewA.isVisible = !binding.ViewA.isVisible
        }

        binding.expandB.setOnClickListener {
            binding.ViewB.isVisible = !binding.ViewB.isVisible
        }

        binding.ivClose.setOnClickListener {
            requireActivity().onBackPressed()
        }

    }


    private fun observers() {
        viewModel.homeSummeryPresentData.observe(requireActivity()) {
            Log.d("winn", "${it.teams.size}")
            setupUI(it)
        }
    }

    private fun setupUI(data: DetailSummery) {
        init()
        binding.tvMatchType.text = "${data.type} League"
        data.teams[0].let {
            binding.tvTitleA.text = it.name
            binding.tvtitle.text = it.name
            binding.tvTotalRunA.text = "${it.score}/${it.wicket}"
            binding.tvTotalOverA.text = "(${it.overs})"
            if (it.isToss) {
                binding.tvtossA.visibility = View.VISIBLE
                binding.tvtossA.text = "${it.name} win toss elect ${data.toss_decision}"
            }
            if (it.isWin) {
                binding.tvWinBy.text = "${it.name} wins"
            }
        }
        data.teams[1].let {
            binding.tvTitleB.text = it.name
            binding.tvBtitle.text = it.name
            binding.tvTotalRunB.text = "${it.score}/${it.wicket}"
            binding.tvTotalOverB.text = "(${it.overs})"
            if (it.isToss) {
                binding.tvtossB.visibility = View.VISIBLE
                binding.tvtossB.text = "${it.name} win toss elect ${data.toss_decision}"
            }
            if (it.isWin) {
                binding.tvWinBy.text = "${it.name} wins"
            }
        }

        data.firstInnings.batsmanList.let {
            batAdapterA.setList(it)
            batAdapterA.notifyDataSetChanged()
        }

        data.firstInnings.bowlerList.let {
            bowlAdapterA.setList(it)
            bowlAdapterA.notifyDataSetChanged()
        }

        data.secondInnings.batsmanList.let {
            batAdapterB.setList(it)
            batAdapterB.notifyDataSetChanged()
        }

        data.secondInnings.bowlerList.let {
            bowlAdapterB.setList(it)
            bowlAdapterB.notifyDataSetChanged()
        }


    }

    private fun init() {
        binding.rvBatA.apply {
            this.layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL, false
            )
            this.adapter = batAdapterA
        }
        binding.rvBowlA.apply {
            this.layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL, false
            )
            this.adapter = bowlAdapterA
        }

        binding.rvBatB.apply {
            this.layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL, false
            )
            this.adapter = batAdapterB
        }
        binding.rvBowlB.apply {
            this.layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL, false
            )
            this.adapter = bowlAdapterB
        }
    }

}