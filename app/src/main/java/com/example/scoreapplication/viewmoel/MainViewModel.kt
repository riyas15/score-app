package com.example.scoreapplication.viewmoel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.scoreapplication.data.model.DetailSummery
import com.example.scoreapplication.data.model.HomeSummery
import com.example.scoreapplication.data.model.MatchModel
import com.example.scoreapplication.data.repo.MatchRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    @ApplicationContext val context: Context,
    private val repo: MatchRepo
) : ViewModel() {

    private val _homeData: MutableLiveData<MatchModel> = MutableLiveData()
    val homeData: LiveData<MatchModel> = _homeData

    private val _homePresentData: MutableLiveData<HomeSummery> = MutableLiveData(HomeSummery())
    val homePresentData: LiveData<HomeSummery> = _homePresentData

    private val _homeSummeryPresentData: MutableLiveData<DetailSummery> = MutableLiveData()
    val homeSummeryPresentData: LiveData<DetailSummery> = _homeSummeryPresentData

    init {
        viewModelScope.launch {
            getMatchData()
            getHomeSummery()
            getDetailsSummery()
        }
    }

    private suspend fun getMatchData(){
        repo.readMatchData().collect{
            _homeData.value = it
        }
    }

    private suspend fun getHomeSummery(){
        repo.getHomeSummery().collect {
            _homePresentData.value = it
        }
    }

    private suspend fun getDetailsSummery(){
        repo.getHomeDetilsSummery().collect {
            _homeSummeryPresentData.value = it
        }
    }

}