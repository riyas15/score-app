<br/>
<p align="center">
  <h3 align="center">My Score App</h3>

<img src="screenshot/home.png" width="420" >
<img src="screenshot/details.png" width="420" >



  <h5 align="center"> Match Summary Page </h5>
    <p align="center">
• Utilize the provided JSON data to dynamically generate a visually appealing
match summary.
• Include critical information such as total runs scored by each team, overs
bowled, and the winning margin.
• Ensure that the summary encapsulates all essential details for a
comprehensive overview of the match.
</p>
<h5 align="center"> Scorecard Page </h5>
<p>
• Design a separate scorecard page accessible from the summary.
• Populate the scorecard with detailed information, including individual player
statistics such as runs, balls faced, 4s, 6s, and strike rate.
• Display bowlers' details from the opposing team, covering runs conceded,
overs bowled, wickets taken, and economy.
</p>
    <br/>
    <br/>
</p>



